import React from 'react';
import FindWeather from "./components/find-weather/FindWeather";
import City from "./components/cityPage/City";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

import './App.css';

class App extends React.Component {

  render() {
    let apiKey = "bbd865f76cb026610862335863d2bc31";


    return (
      <div className="app-container">
        <Router basepath="/weatherapp">
          <div>
            <div>
              <Redirect from="/weatherapp" to="/weatherapp/copenhagen" />
              <Link to="/weatherapp/copenhagen"> <img className="logo" src={require("./img/cloudy.png")} alt="logo" /></Link>
              <nav>
                <Link to="/weatherapp/copenhagen">Copenhagen</Link>
                <Link to="/weatherapp/prague">Prague</Link>
                <Link to="/weatherapp/find-weather" >{window.innerWidth >= 550 ? "Find weather" : "Search"}</Link>
              </nav>
            </div>
            <Switch>
              <Route
                key="Copenhagen"
                exact path="/weatherapp/copenhagen">
                <City name="Copenhagen" apiKey={apiKey} city="Copenhagen" country="dk" />
              </Route>
              <Route
                key="Prague"
                exact path="/weatherapp/prague">
                <City name="Prague" apiKey={apiKey} city="prague" country="cz" />
              </Route>
              <Route
                key="find-weather"
                exact path="/weatherapp/find-weather">
                <FindWeather apiKey={apiKey} />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
