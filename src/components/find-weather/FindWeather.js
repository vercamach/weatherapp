import React from 'react';
import './find-weather.css';
import FutureWeather from "../future-weather/FutureWeather";
import Form from "../form/Form";
import CurrentWeather from "../current-weather/CurrentWeather";


class FindWeather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullData: [],
            dailyData: [],
            currentData: null,
            inputError: null,
            formIsOpen: true
        };
    }
    render() {
        let handleOnSubmit = async (e) => {
            e.preventDefault();
            let city = e.target.elements.city.value;
            let country = e.target.elements.country.value;
            let fetchFutureWeather = await fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${city},${country}&appid=${this.props.apiKey}&units=metric`);
            await fetchFutureWeather.json().then(data => {
                if (data.cod === "200") {
                    this.setState({ currentData: data })
                    let dailyData = data.list.filter(weatherList => weatherList.dt_txt.includes("15:00:00"))
                    this.setState({
                        fullData: data.list,
                        dailyData: dailyData,
                        inputError: null,
                        formIsOpen: false
                    })
                } else if (data.cod !== "200") {
                    this.setState({ inputError: data.message })
                }
            })
        }
        return (
            <>
                {this.state.formIsOpen ?

                    <Form handleOnSubmit={handleOnSubmit} /> : <img className="form-icon" src={require("../../img/icons/search.svg")} alt="serch" height="30px" onClick={() => this.setState({ formIsOpen: true })} />}
                {this.state.inputError !== null && <p className="error">{this.state.inputError}</p>}
                {!this.state.formIsOpen && <div className="form-weather-wrapper">
                    <CurrentWeather data={this.state.currentData} />
                    <div className="future-weather-wrapper" >
                        {this.state.dailyData.map((day, i) => {
                            return (
                                <FutureWeather key={i} day={day} />
                            )
                        })}
                    </div>
                </div>}
            </>
        );
    }
}

export default FindWeather;
