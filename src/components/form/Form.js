import React from 'react';
import "./form.css";


class Form extends React.Component {
    render() {
        return (

            <div className="form-wrapper">
                <form method="POST" onSubmit={this.props.handleOnSubmit}>
                    <div className="input-wrapper">
                        <label htmlFor="city">City</label>
                        <input type="text" id="city" name="city" required />
                    </div>
                    <div className="input-wrapper">
                        <label htmlFor="country">Country</label>
                        <input type="text" id="country" name="country" required />
                    </div>
                    <button type="submit" variant="contained" color="primary">Search</button>
                </form>
            </div>
        )
    }
}

export default Form;