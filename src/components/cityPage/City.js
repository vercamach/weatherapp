import React from "react";
import FutureWeather from "../future-weather/FutureWeather"
import CurrentWeather from "../current-weather/CurrentWeather"
import "./city.css"
export default class City extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cityData: null,
            inputError: null,
            dailyData: null
        };
    }
    componentDidMount() {
        fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${this.props.city},${this.props.country}&appid=${this.props.apiKey}&units=metric`)
            .then(response => response.json()).then(data => {
                if (data.cod === "200") {
                    let dailyData = data.list.filter(weatherList => weatherList.dt_txt.includes("15:00:00"))
                    this.setState({
                        cityData: data,
                        dailyData: dailyData,
                        inputError: null
                    })
                } else {
                    this.setState({ inputError: data.message })

                }
            })
    }

    render() {
        return (
            <div>
                {this.state.cityData === null ? "...lodading" :
                    <div className="city-wrapper">
                        <CurrentWeather data={this.state.cityData} />
                        <div className="future-weather-wrapper" >
                            {this.state.dailyData.map((day, i) => {
                                return (
                                    <FutureWeather key={i} day={day} />
                                )
                            })}
                        </div>
                    </div>
                }

            </div >
        )
    }

}