import React from "react";
import "./current-weather.css"


export default class CurrentWeather extends React.Component {
    render() {
        const { temp, temp_max, temp_min, humidity } = this.props.data ? this.props.data.list[0].main : "loading"
        const { name, sunrise, sunset } = this.props.data ? this.props.data.city : "loading"
        const { icon, description } = this.props.data ? this.props.data.list[0].weather[0] : "loading"
        const { speed } = this.props.data ? this.props.data.list[0].wind : "loading"
        const getTime = (timestamp) => new Date(timestamp * 1000).toLocaleTimeString({ hour12: false, hour: '2-digit', minute: '2-digit' });
        return (
            <>
                {
                    this.props.data !== null &&
                    <div className="current-weather-wrapper">
                        <div className="center-box">
                            <h1 className="city-name">{name}</h1>
                            <div className="temperature-description">
                                <img className="current-weather-icon" src={require("../../img/icons/" + icon + ".svg")} alt="icon" height="80px" />
                                <p className="temperature-text">{temp.toFixed(1)}°C</p>
                            </div>
                            <h2>{description}</h2>
                        </div>
                        <div className="center-box">
                            <div>
                                <img src={require("../../img/icons/max-temp.svg")} alt="maximum-temperature" height="25px" />
                                <h3>{temp_max.toFixed(1)}°C</h3>
                            </div>
                            <div className="top-margin-box">
                                <img src={require("../../img/icons/min-temp.svg")} alt="minimum-temperature" height="25px" />
                                <h3>{temp_min.toFixed(1)}°C</h3>
                            </div>
                        </div>
                        <div className="center-box sunrise-sunset">
                            <div>
                                <img src={require("../../img/icons/sunrise.svg")} alt="sunrise-icon" height="25px" />

                                <h3>{getTime(sunrise)}</h3>
                            </div>
                            <div className="top-margin-box">
                                <img src={require("../../img/icons/sunset.svg")} alt="sunset-icon" height="21px" />

                                <h3>{getTime(sunset)}</h3>
                            </div>
                        </div>

                        <div className="center-box humidity-wind">
                            <div>
                                <img src={require("../../img/icons/humidity.svg")} alt="humidity-icon" height="25px" />
                                <h3>{humidity}%</h3>
                            </div>
                            <div className="top-margin-box">
                                <img src={require("../../img/icons/wind.svg")} alt="wind-icon" height="20px" />

                                <h3>{speed}</h3>
                            </div>
                        </div>
                    </div>
                }
            </>
        )

    }
}