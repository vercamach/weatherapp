import React from "react";
import "./future-weather.css";

export default class FutureWeather extends React.Component {
    render() {
        const { temp } = this.props.day.main
        const { description, icon } = this.props.day.weather[0]
        const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sut"]
        let date = new Date(this.props.day.dt_txt)
        let weekDay = weekdays[date.getDay()];
        return (
            <div className="future-weather-box">
                <p>{weekDay}</p>
                <img className="future-weather-icon" src={require(`../../img/icons/${icon}.svg`)} alt="weather-icon" height="45" />
                <p className="future-weather-description">{description}</p>
                <p>{temp.toFixed(1)}°C</p>
            </div>
        )
    }
}